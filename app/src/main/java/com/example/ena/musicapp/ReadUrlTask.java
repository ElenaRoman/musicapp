package com.example.ena.musicapp;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by ena on 02/07/15.
 */
public class ReadUrlTask extends AsyncTask <String, Integer, String> {
    private MainActivity mainActivity;

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected String doInBackground(String... params) {
        String row = null;

        try {
            URL url = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            StringBuffer stringBuffer = new StringBuffer();

            while ((row = bufferedReader.readLine()) != null) {
                if (row.contains("http")) {
                    connection.disconnect();
                    bufferedReader.close();
                    inputStream.close();

                    return row;
                }
                stringBuffer.append(row);
            }

            connection.disconnect();
            bufferedReader.close();
            inputStream.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {
            mainActivity.mediaPlayer.setDataSource(s);
            mainActivity.mediaPlayer.prepare();
            mainActivity.mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
